# Buteo Synchronization Framework Contacts Synchronization Plugins

This project provides plugins for the [Buteo Synchronization Framework][1] in
order to synchronize contacts between remote services and local QtContacts
[addressbook-service][2] backend.

Currently there is only a plugin for Google Contacts.


[1]: https://github.com/sailfishos/buteo-syncfw/
[2]: https://gitlab.com/ubports/development/core/address-book-service
